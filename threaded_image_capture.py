from picamera import PiCamera
import time
import datetime
#import math
from gps3.agps3threaded import AGPS3mechanism
#import signal
import os
import threading 
import faulthandler
#import sys
import zmq
import subprocess


def capture(camera):
    today = datetime.datetime.now() 
    new_dir = '/home/pi/Pictures/'+ today.strftime('%Y-%m-%d-%H_%M_%S')
    try:
        if not os.path.isdir(new_dir):
            os.mkdir(new_dir)
        else:
            new_dir = new_dir + "Boom"
            os.mkdir(new_dir)
    except:
        print('Failed to create image directory.')
    agps_thread = AGPS3mechanism()
    agps_thread.stream_data(host='192.168.4.1', port=2947)
    agps_thread.run_thread()
    while agps_thread.data_stream.mode == 'n/a':
        time.sleep(.1)    
    camera.capture_sequence(imagename(new_dir, agps_thread, capture_comm, poller),  use_video_port=True)

def shutdown():
    command = 'usr/bin/sudo /sbin/shutdown -r now'
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print(output)

def imagename(directory, agps_thread, capture_comm, poller):
    imaging = True
    counter = 0
    while imaging:
        while agps_thread.data_stream.speed < 0:
            time.sleep(0.5)
        timestamp = datetime.datetime.now()
        filename = f'{directory}/OSM_L_{timestamp}_{counter}.jpeg'
        yield filename
        capture_comm.send((counter).to_bytes(5, 'little'))
        message = dict(poller.poll(0.01))
        if capture_comm in message:
            stop_signal = capture_comm.recv()
            if stop_signal == b"stop":
                imaging = False
            elif stop_signal == b'shutdown':
                sys.exit()
        counter += 1
        time.sleep(0.75)

def main(args):
    print(args)
    context = zmq.Context.instance()
    capture_comm = context.socket(zmq.PAIR)
    capture_comm.connect("inproc://image_capture")
    
    poller = zmq.Poller()
    poller.register(capture_comm, zmq.POLLIN)

    IMG_DIRECTION_CORRECT_L = 0
    SEC_BETWEEN_IMAGE = 3
    begin_signal = False
    current_image_number = 0

    camera = PiCamera()
    #camera.sensor_mode = 2
    camera.resolution = (3280, 2464)
    camera.rotation = 90
    #camera.exposure_mode = 'sports'
    shutdown = False
    while shutdown == False:
        message = dict(poller.poll(1))
        if capture_comm in message:
            signal = capture_comm.recv()
            if signal == b"start":
                imaging = True
                capture('CarCamTest', camera)
            elif signal == b'shutdown':
                shutdown = True
            elif signal == b'stop':
                print('Imaging Already Stopped.')
            else:
                print('Imaging Command Not Recognized.')
        else:
            print('No messages Received From Parent Process')
    camera.close()
    agps_thread.stop()

if __name__ == '__main__':
    main('Carcam Imager')

