#!/usr/bin/python3

import zmq
import sys
import pickle
import socket
import time


class StatusUpdate(object):

    """
    Message class for all nodes in the carcam network to send commands/status updates
    """
    def __init__(self, carcam_id='UNK Hostname', command_role=False, ready_status=False, imaging_status=False, image_number=0, shutdown_status=False):
        self.carcam_id = carcam_id 
        self.command_role = command_role
        self.ready_status = ready_status
        self.imaging_status = imaging_status
        self.image_number = image_number
        self.shutdown_status = shutdown_status

def send_status(status_update, zmq_socket):
    p_status = pickle.dumps(status_update)
#    zmq_socket.send_pyobj(p_status)
    zmq_socket.send_multipart([bytearray(str(status_update.command_role), 'utf-8'), p_status])

def receive_status(zmq_socket):
    [topic, p_status] = zmq_socket.recv_multipart()
    status_update = pickle.loads(p_status)
    return status_update

def parse_status(status_update, 
        criteria, 
        left_update_message,
        right_update_message,
        front_update_message,
        back_update_message):
    
    if criteria == 'ready_status':
        try:
            update_bool = status_update.ready_status
            if update_bool == True:
                message = "Ready"
            elif update_bool == False:
                message = "Not Rdy"
            else:
                print('Ready Status not recognized')
        except NameError:
            print("Update not in the correct data structure")
    elif criteria == 'imaging_status':
        try:
            update_bool = status_update.imaging_status
            if update_bool == True:
                message = "Imging"
            elif update_bool == False:
                message = "Not Img"
            else:
                print('Imaging Status not recognized')
        except NameError:
            print("Update not in the correct data structure")
    elif criteria == 'image_number':
        try:
            update_int = status_update.image_number
            if isinstance(update_int, int):
                message = str(update_int)
                print("Image number is an integer and is: {}".format(message))
            else:
                print('Image Number is not an Integer')
        except NameError:
            print("Update not in the correct data structure")
    elif criteria == 'shutdown_status':   
        try:
            update_bool = status_update.shutdown_status
            if update_bool == True:
                message = "PWRDWN"
            elif update_bool == False:
                message = "PWRUP"
            else:
                print('Shutdown Status not recognized')
        except NameError:
            print("Update not in the correct data structure")
    else:
        print('Criteria not recognized')
        return
    if status_update.carcam_id == 'carcaml':
        left_update_message = message
        
    elif status_update.carcam_id == 'carcamr':
        right_update_message = message
        
    elif status_update.carcam_id == 'carcamf':
        front_update_message = message
        
    elif status_update.carcam_id == 'carcamb':
        back_update_message = message
        
    else:
        print("Carcam_Id not recognized, cannot write to LCD")
    message_list =[left_update_message, 
            right_update_message, 
            front_update_message, 
            back_update_message]
    return message_list


def write_status(lcd, left_update_message, 
        right_update_message, 
        front_update_message, 
        back_update_message):
    lcd.clear()
    lcd.cursor_pos = (0,0)
    lcd.write_string('L:' + left_update_message)
    lcd.cursor_pos = (0,8)
    lcd.write_string('R:' + right_update_message)
    lcd.cursor_pos = (1,0)
    lcd.write_string('F:' + front_update_message)
    lcd.cursor_pos = (1,8)
    lcd.write_string('B:' + back_update_message)
    time.sleep(2)
    
def parse_button(lcd, button_color, carcamhq_status):

    if button_color == b"white" and carcamhq_status.ready_status == True:
        carcamhq_status.imaging_status = True
        lcd.clear()
        lcd.write_string('Started')
        print('Started')
        time.sleep(2)
    elif button_color == b"lblue" and carcamhq_status.ready_status == True:
        subprocess.call(['gpsctl', '-c', '0.5'])
        lcd.clear()
        lcd.write_string('Freq Set to: 0.5')
        time.sleep(2)
    elif button_color == "rblue" and carcamhq_status.ready_status == True:
        subprocess.call(['gpsctl', '-c', '1'])
        lcd.clear()
        lcd.write_string('Freq Set to: 1')
        time.sleep(2)
    elif button_color == b"red" and carcamhq_status.imaging_status == True:
        carcamhq_status.imaging_status = False
        lcd.clear()
        lcd.write_string('Imaging Stopped')
        time.sleep(2)
    elif button_color == b"red" and carcamhq_status.imaging_status == False:
        carcamhq_status.shutdown_status = True
        lcd.clear()
        lcd.write_string('Shutting Down')
        time.sleep(2)
    return carcamhq_status







