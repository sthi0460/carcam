import subprocess
import gpsd
from RPLCD.gpio import CharLCD
import time
from gpiozero import Button  
from RPi import GPIO
import socket
import struct

multicast_group = ('224.0.0.251', 10000)
MESSAGE_START = "BeginCollecting"
MESSAGE_END = "StopCollecting"
MESSAGE_SHUTDOWN = "Shutdown"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(0.2)
ttl = struct.pack('b', 1)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

button_red = Button(26)
button_lblue = Button(19)
button_rblue = Button(13)
button_white = Button(6)

lcd = CharLCD(cols=16, rows=2, pin_rs=24, pin_e=23, pins_data=[17, 18, 27, 22], numbering_mode=GPIO.BCM)

gpsd.connect()

lcd.clear()
lcd.write_string('CarCam HQ')
time.sleep(2)
lcd.clear()
collection_status = False
while True:
    lcd.clear()
    lcd.write_string('Status')
    if button_white.is_pressed:
        try:
            sock.sendto(bytes(MESSAGE_START, 'utf-8'), multicast_group)
            collection_status = True
            lcd.clear()
            lcd.write_string('Started')
            print('Started')
            time.sleep(2)
        except:
            lcd.clear()
            lcd.write_string('Failed to Start')
            time.sleep(2)
    elif button_lblue.is_pressed and collection_status == True:
        try:
            subprocess.call(['gpsctl', '-c', '0.5'])
            lcd.clear()
            lcd.write_string('Freq Set to: 0.5')
            time.sleep(2)
        except:
            lcd.clear()
            lcd.write_string('Failed to set freq')
            time.sleep(2)
    elif button_rblue.is_pressed and collection_status == True:
        try:
            subprocess.call(['gpsctl', '-c', '1'])
            lcd.clear()
            lcd.write_string('Freq Set to: 1')
            time.sleep(2)
        except:
            lcd.clear()
            lcd.write_string('Failed to set freq')
            time.sleep(2)
    elif button_red.is_pressed and collection_status == True:
        try:
            sock.sendto(bytes(MESSAGE_END, 'utf-8'), multicast_group)
            collection_status = False
            lcd.clear()
            lcd.write_string('Stopped')
            time.sleep(2)
        except:
            lcd.clear()
            lcd.write_string('Failed to stop')
            time.sleep(2)
    elif button_red.is_pressed and collection_status == False:
        lcd.clear()
        lcd.write_string('Shutting Down')
        time.sleep(2)
        subprocess.call(['sudo', 'shutdown', 'now'])
    else:
        pass
        next
#        lcd.clear()
#        lcd.write_string('Try Again')
#        time.sleep(1)
#        lcd.clear()
#    menu_navigation(current_menu_position)
#    button_white.when_pressed = menu_dic[menu_options[current_menu_position]]


#    time.sleep(1)
#    lcd.clear()

#        move to next menu item        
#    lcd.clear()
#    time.sleep(1)
#    lcd.message(gpsd_ststus)



