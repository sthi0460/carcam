#!/usr/bin/python3
from time import sleep
import socket
#import struct
import threading
import signal
import subprocess
from threaded_image_capture import main
import zmq
import carcamlib

def main():
    context = zmq.Context.instance()
    subscriber = context.socket(zmq.SUB)
    subscriber.connect("tcp://192.168.4.1:5557")
    subscriber.setsockopt(zmq.SUBSCRIBE, b"True")
    
    publisher = context.socket(zmq.PUB)
    publisher.connect("tcp://192.168.4.1:5556")
    
    capture_comm = context.socket(zmq.PAIR)
    capture_comm.bind("inproc://image_capture")

    poller = zmq.Poller()
    poller.register(subscriber, zmq.POLLIN)
    poller.register(capture_comm, zmq.POLLIN)
#    thread_list = []
    hostname = socket.gethostname()
    imager_status = carcamlib.StatusUpdate(carcam_id = hostname)
#    collecting = False
    #thread_list.append(threading.Thread(target=capture, args=('Carcam_Signal',))) 
    imaging_thread = threading.Thread(target=main, args=('Carcam_Signal',))
    imager_status.ready_status = True
    print(imager_status.command_role)
    carcamlib.send_status(imager_status, publisher)
    while True:
        for recv_iter in range(1,10):
            try:
                print("Begin Poll")
                status_updates = dict(poller.poll())
                print("End Poll")
            except KeyboardInterrupt:
                break
            if subscriber in status_updates:
                update = carcamlib.receive_status(subscriber)
                if update.imaging_status == True and imager_status.imaging_status == False:
                    print('received begin signal') 
                    capture_comm.send(b'start')
                    imager_status.imaging_status = True
                elif (imager_status.imaging_status == True 
                        and update.imaging_status == False):
                    print('received stop signal')
                    capture_comm.send(b'stop')                    
                    imager_status.imaging_status = False
                else:
                    print('nothing')
                if update.shutdown_status == True:
                    print('received shutdown signal')
                    capture_comm.send(b'shutdown')
                    subprocess.run(['sudo', 'shutdown', 'now'])
            if capture_comm in status_updates:
                image_num_from_thread = capture_comm.recv() 
                image_num_from_thread = int.from_bytes(image_num_from_thread, 'little', signed=False)
                print("Image num from thread: "+ str(image_num_from_thread))
                imager_status.image_number = image_num_from_thread
                print("Imager_status.imagenumber: "+ str(imager_status.image_number))
        carcamlib.send_status(imager_status, publisher)
        sleep(2)
if __name__ == '__main__':
    main()


